package com.example.myapplication.behaviours;

import android.graphics.PointF;

import com.example.myapplication.figures.Figure;
import com.example.myapplication.MainActivity;
import com.example.myapplication.figures.Rectangle;
import com.example.myapplication.utils.PointD;

public class RectBehaviour extends DrawingBehaviour {
    public RectBehaviour(MainActivity mainActivity) {
        super(mainActivity, 2);
    }

    @Override
    public Figure createFigure(int color) {
        PointD a = points.get(0);
        PointD b = points.get(1);
        if (a.x > b.x) {
            double x = a.x;
            a.x = b.x;
            b.x = x;
        }
        if (a.y > b.y) {
            double y = a.y;
            a.y = b.y;
            b.y = y;
        }
        return new Rectangle(a, new PointD(b.x - a.x, b.y - a.y), color);
    }
}
