package com.example.myapplication.behaviours;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;

import com.example.myapplication.R;
import com.example.myapplication.figures.Figure;
import com.example.myapplication.MainActivity;
import com.example.myapplication.interfaces.Drawing;
import com.example.myapplication.memento.MyCanvasCaretaker;
import com.example.myapplication.utils.PointD;

import java.util.ArrayList;
import java.util.List;

public abstract class DrawingBehaviour implements Drawing {
    private int state;
    private int countPoints;
    private MainActivity mainActivity;
    protected List<PointD> points;

    public DrawingBehaviour(MainActivity mainActivity, int countPoints) {
        this.mainActivity = mainActivity;
        this.countPoints = countPoints;
        points = new ArrayList<>();
    }

    public void onClick(MyCanvasCaretaker myCanvasCaretaker, PointD position) {
        if (state == 0)
            mainActivity.startDraw();
        points.add(position);
        state++;
        if (state == countPoints) {
            myCanvasCaretaker.drawFigure(createFigure(mainActivity.getSelColor()));
            mainActivity.stopDraw();
            points.clear();
            state = 0;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.rgb(255, 128, 0));
        for (PointD pointD : points) {
            canvas.drawCircle((float) pointD.x, (float) pointD.y, 5, paint);
        }
    }

    public abstract Figure createFigure(int color);
}
