package com.example.myapplication.behaviours;

import com.example.myapplication.figures.Figure;
import com.example.myapplication.MainActivity;
import com.example.myapplication.figures.Triangle;

public class TriangleBehaviour extends DrawingBehaviour {
    public TriangleBehaviour(MainActivity mainActivity) {
        super(mainActivity, 3);
    }

    @Override
    public Figure createFigure(int color) {
        return new Triangle(points.get(0), points.get(1), points.get(2), color);
    }
}
