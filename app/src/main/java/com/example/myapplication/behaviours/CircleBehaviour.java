package com.example.myapplication.behaviours;

import android.graphics.PointF;

import com.example.myapplication.figures.Circle;
import com.example.myapplication.figures.Figure;
import com.example.myapplication.MainActivity;
import com.example.myapplication.utils.Math;
import com.example.myapplication.utils.PointD;

public class CircleBehaviour extends DrawingBehaviour {
    public CircleBehaviour(MainActivity mainActivity) {
        super(mainActivity, 2);
    }

    @Override
    public Figure createFigure(int color) {
        PointD a = points.get(0);
        PointD b = points.get(1);
        double radius = Math.distance(a, b);
        return new Circle(a, (float) radius, color);
    }
}
