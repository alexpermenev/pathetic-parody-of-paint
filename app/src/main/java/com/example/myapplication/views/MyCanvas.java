package com.example.myapplication.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.myapplication.MainActivity;
import com.example.myapplication.behaviours.DrawingBehaviour;
import com.example.myapplication.figures.Figure;
import com.example.myapplication.memento.MyCanvasCaretaker;
import com.example.myapplication.utils.Math;
import com.example.myapplication.utils.PointD;

public class MyCanvas extends View {
    private static final int CELL_SIZE = 20;

    private Paint paint;
    private MyCanvasCaretaker myCanvasCaretaker;
    private DrawingBehaviour drawingBehaviour;
    private PointD lastDownTouch;
    private PointD lastMoveTouch;
    private Figure touchFigure;
    private boolean isMove;

    public MyCanvas(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setColor(Color.BLACK);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    lastDownTouch = new PointD(motionEvent.getX(), motionEvent.getY());
                    if (isMove) {
                        lastMoveTouch = lastDownTouch;
                        touchFigure = myCanvasCaretaker.raycast(lastDownTouch);
                        if (touchFigure != null) {
                            myCanvasCaretaker.save();
                        }
                    }
                }
                if (isMove) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_MOVE && touchFigure != null) {
                        PointD buf = new PointD(motionEvent.getX(), motionEvent.getY());
                        PointD dir = new PointD(buf.x - lastMoveTouch.x, buf.y - lastMoveTouch.y);
                        myCanvasCaretaker.moveFigure(touchFigure, dir);
                        lastMoveTouch = buf;
                        invalidate();
                    }
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    PointD buf = new PointD(motionEvent.getX(), motionEvent.getY());
                    if (Math.distance(lastDownTouch, buf) < 2) {
                        if (isMove) {
                            if (touchFigure != null) {
                                myCanvasCaretaker.undo();
                            }
                        }
                        else {
                            setPoint(buf);
                        }
                    }
                }
                return true;
            }
        });
        myCanvasCaretaker = new MyCanvasCaretaker();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
        int i = 0;
        while (i <= getHeight()) {
            canvas.drawLine(0, i, getWidth() - 1, i, paint);
            i += CELL_SIZE;
        }
        i = 0;
        while (i <= getWidth()) {
            canvas.drawLine(i, 0, i, getHeight() - 1, paint);
            i += CELL_SIZE;
        }
        myCanvasCaretaker.draw(canvas);
        drawingBehaviour.draw(canvas);
    }

    private void setPoint(PointD position) {
        drawingBehaviour.onClick(myCanvasCaretaker, position);
        invalidate();
    }

    public void setDrawingBehaviour(DrawingBehaviour drawingBehaviour) {
        this.drawingBehaviour = drawingBehaviour;
    }

    public void undo() {
        myCanvasCaretaker.undo();
        invalidate();
    }

    public void switchMove() {
        isMove = !isMove;
    }
}
