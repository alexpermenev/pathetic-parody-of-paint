package com.example.myapplication.utils;

import android.graphics.PointF;

public class Math {
    public static int negativeColor(int color) {
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = color & 0xFF;
        r = 255 - r;
        g = 255 - g;
        b = 255 - b;
        return (r << 16) + (g << 8) + b + 0xFF000000;
    }

    public static double squareTriangle(PointD ta, PointD tb, PointD tc) {
        return java.lang.Math.abs((tb.x - ta.x) * (tc.y - ta.y) - (tc.x - ta.x) * (tb.y - ta.y)) / 2;
    }

    public static double distance(PointD a, PointD b) {
        double x = b.x - a.x;
        double y = b.y - a.y;
        return java.lang.Math.sqrt(x * x + y * y);
    }
}
