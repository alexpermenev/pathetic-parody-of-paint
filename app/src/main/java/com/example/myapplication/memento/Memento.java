package com.example.myapplication.memento;

import com.example.myapplication.figures.Figure;

import java.util.ArrayList;
import java.util.List;

public class Memento {
    private List<Figure> figureHistory;

    public Memento(List<Figure> state) {
        figureHistory = new ArrayList<>();
        for (Figure figure : state) {
            figureHistory.add(figure.clone());
        }
    }

    public List<Figure> getState() {
        return figureHistory;
    }
}
