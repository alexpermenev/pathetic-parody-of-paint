package com.example.myapplication.memento;

import android.graphics.Canvas;
import android.graphics.PointF;

import com.example.myapplication.figures.Figure;
import com.example.myapplication.interfaces.Drawing;
import com.example.myapplication.utils.PointD;

import java.util.ArrayList;
import java.util.List;

public class CanvasOriginator implements Drawing {
    private List<Figure> figureHistory;

    public CanvasOriginator() {
        figureHistory = new ArrayList<>();
    }

    public Memento save() {
        return new Memento(figureHistory);
    }

    public void restore(Memento m) {
        figureHistory = m.getState();
    }

    public void addFigure(Figure figure) {
        figureHistory.add(figure);
    }

    public Figure raycast(PointD touch) {
        for (int i = figureHistory.size() - 1; i >= 0; i--) {
            if (figureHistory.get(i).isPointBounds(touch)) {
                return figureHistory.get(i);
            }
        }
        return null;
    }

    public void moveFigure(Figure figure, PointD dir) {
        figure.translate(dir);
    }

    @Override
    public void draw(Canvas canvas) {
        for (Figure figure : figureHistory) {
            figure.draw(canvas);
        }
    }
}
