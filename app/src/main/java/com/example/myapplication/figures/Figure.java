package com.example.myapplication.figures;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import com.example.myapplication.interfaces.Drawing;
import com.example.myapplication.utils.PointD;

public abstract class Figure implements Drawing {
    protected Paint paint;
    protected PointD position;
    protected int countPoints;

    public Figure(PointD position, int color) {
        this.position = position;
        paint = new Paint();
        paint.setColor(color);
    }

    public void translate(PointD dir) {
        position = new PointD(position.x + dir.x, position.y + dir.y);
    }

    public int getCountPoints() {
        return countPoints;
    }

    public abstract Figure clone();
    public abstract boolean isPointBounds(PointD point);
}
