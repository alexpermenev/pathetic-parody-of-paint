package com.example.myapplication.figures;

import android.graphics.Canvas;
import android.graphics.PointF;

import androidx.annotation.NonNull;

import com.example.myapplication.utils.PointD;

public class Circle extends Figure {
    private float radius;

    public Circle(PointD position, float radius, int color) {
        super(position, color);
        this.radius = radius;
    }

    public Circle(@NonNull Circle circle) {
        this(new PointD(circle.position), circle.radius, circle.paint.getColor());
    }

    @Override
    public Figure clone() {
        return new Circle(this);
    }

    @Override
    public boolean isPointBounds(PointD point) {
        double x = point.x - position.x;
        double y = point.y - position.y;
        return x * x + y * y < radius * radius;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle((float) position.x, (float) position.y, radius, paint);
    }
}
