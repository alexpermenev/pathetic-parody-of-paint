package com.example.myapplication.figures;

import android.graphics.Canvas;

import androidx.annotation.NonNull;

import com.example.myapplication.utils.PointD;

public class Rectangle extends Figure {
    private PointD size;

    public Rectangle(PointD position, PointD size, int color) {
        super(position, color);
        this.size = size;
    }

    public Rectangle(@NonNull Rectangle rectangle) {
        this(new PointD(rectangle.position), rectangle.size, rectangle.paint.getColor());
    }

    @Override
    public Figure clone() {
        return new Rectangle(this);
    }

    @Override
    public boolean isPointBounds(PointD point) {
        return point.x > position.x &&
                point.x < position.x + size.x &&
                point.y > position.y &&
                point.y < position.y + size.y;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect((float) position.x, (float) position.y, (float) (position.x + size.x), (float) (position.y + size.y), paint);
    }
}
