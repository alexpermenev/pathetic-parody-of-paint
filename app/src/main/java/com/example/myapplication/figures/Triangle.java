package com.example.myapplication.figures;

import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.PointF;

import androidx.annotation.NonNull;

import com.example.myapplication.utils.Math;
import com.example.myapplication.utils.PointD;

public class Triangle extends Figure {
    private PointD toB;
    private PointD toC;

    public Triangle(PointD a, PointD b, PointD c, int color) {
        super(a, color);
        this.toB = new PointD(b.x - a.x, b.y - a.y);
        this.toC = new PointD(c.x - a.x, c.y - a.y);
    }

    public Triangle(@NonNull Triangle triangle) {
        this(new PointD(triangle.position), triangle.getB(), triangle.getC(), triangle.paint.getColor());
    }

    private PointD getB() {
        return new PointD(position.x + toB.x, position.y + toB.y);
    }

    private PointD getC() {
        return new PointD(position.x + toC.x, position.y + toC.y);
    }

    @Override
    public Figure clone() {
        return new Triangle(this);
    }

    @Override
    public boolean isPointBounds(PointD point) {
        PointD bufB = getB();
        PointD bufC = getC();
        double a = Math.squareTriangle(point, position, bufB);
        double b = Math.squareTriangle(point, bufB, bufC);
        double c = Math.squareTriangle(point, bufC, position);
        return a + b + c - Math.squareTriangle(position, bufB, bufC) < 0.001f;
    }

    @Override
    public void draw(Canvas canvas) {
        Path path = new Path();
        path.moveTo((float) position.x, (float) position.y);
        path.lineTo((float) (position.x + toB.x), (float) (position.y + toB.y));
        path.lineTo((float) (position.x + toC.x), (float) (position.y + toC.y));
        path.lineTo((float) position.x, (float) position.y);
        path.close();
        canvas.drawPath(path, paint);
    }
}
