package com.example.myapplication.interfaces;

import android.graphics.Canvas;

public interface Drawing {
    void draw(Canvas canvas);
}
